#!/usr/bin/env bash

docker login registry.gitlab.com
docker build . -t registry.gitlab.com/onacy/docker/openssh-client
docker push registry.gitlab.com/onacy/docker/openssh-client