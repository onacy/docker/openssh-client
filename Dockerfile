FROM alpine

RUN apk --no-cache add bash openssh-client curl rsync git grep findutils

CMD ["/bin/bash"]
